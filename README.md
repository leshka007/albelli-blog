My spectacular blog
==================

## Setup
1. clone the project repository
2. Install docker
3. Install docker-compose
4. Pull images:
```
docker-compose pull
```
5. Install project's dependencies
```
bin/composer install
bin/npm install
```
6. Make a build
```
bin/npm run build
```

## Start project
1. Run
```
docker-compose up -d
```
2. open [start page](http://localhost/)
