<?php

use Albelli\Blog\Http\Handler\AddPostHandler;
use Albelli\Blog\Http\Handler\PostsHandler;

return [
    [ 'GET', '/api/posts', PostsHandler::class ],
    [ 'POST', '/api/posts/add', AddPostHandler::class ]
];
