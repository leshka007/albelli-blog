<?php

use Albelli\Blog\Image\Storage;
use Albelli\Blog\Repository\PostRepository;
use Albelli\Blog\Repository\TagRepository;

return [
    Storage::class => function($c) {
        return new Storage(__DIR__ . '/../storage/images');
    },
    PostRepository::class => function($c) {
        return new PostRepository(__DIR__ . '/../storage/blog');
    },
    TagRepository::class => function($c) {
        return new TagRepository(__DIR__ . '/../storage/tags/db.json');
    }
];
