import React, { Component } from 'react';
import Header from './Core/Header.jsx';
import NewPost from './Blog/NewPost.jsx';
import PostList from "./Blog/PostList.jsx";
import PopularTagList from "./Blog/PopularTagList.jsx";

class App extends Component {
  render() {
    return (
      <div className="container">
          <Header />
          <NewPost />
          <div className="row">
              <div className="span-1-of-3 col-fr col-mr">
                  <PopularTagList />
              </div>
              <div className="span-2-of-3 col-fr col-mr">
                  <PostList />
              </div>
          </div>
      </div>
    );
  }
}

export default App;
