import fetch from 'isomorphic-fetch'
import moment from 'moment'

export const BLOG_REQUEST_POSTS = 'BLOG_REQUEST_POSTS';
export const BLOG_RECEIVE_POSTS = 'BLOG_RECEIVE_POSTS';
export const BLOG_RECEIVE_FAILED = 'BLOG_RECEIVE_FAILED';

export const requestPosts = () => ({
    type: BLOG_REQUEST_POSTS
});

export const receivePosts = (json) => ({
    type: BLOG_RECEIVE_POSTS,
    posts: json.posts.map(function(item) {
        item.createdAt = moment(item.createdAt, 'X');
        return item;
    }),
    tags: json.tags
});

export const receiveFailed = () => ({
    type: BLOG_RECEIVE_FAILED
});

export const fetchPosts = () => {
    return dispatch => {
        dispatch(requestPosts());
        return fetch('/api/posts')
            .then(response => response.json())
            .then((json) => {
                dispatch(receivePosts(json));
            })
            .catch((err) => {
                dispatch(receiveFailed())
            });
    };
};
