import { BLOG_REQUEST_POSTS, BLOG_RECEIVE_POSTS, BLOG_RECEIVE_FAILED } from './BlogActions';

const blogReducer = (
    state = {
        isFetching: false,
        isFetchFailed: false,
        posts: [],
        tags: []
    },
    action
) => {
    switch (action.type) {
        case BLOG_REQUEST_POSTS:
            return Object.assign({}, state, {
                isFetching: true,
                posts: [],
                tags: []
            });
        case BLOG_RECEIVE_POSTS:
            return Object.assign({}, state, {
                isFetching: false,
                posts: action.posts,
                tags: action.tags
            });
        case BLOG_RECEIVE_FAILED:
            return Object.assign({}, state, {
                isFetching: false,
                isFetchFailed: true
            });
        default:
            return state;
    }
};

export default blogReducer
