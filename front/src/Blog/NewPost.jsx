import React, { Component } from 'react';
import { addPost } from './NewPostActions';
import { connect } from "react-redux";
import PropTypes from 'prop-types'

class NewPost extends Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.addPost(new FormData(event.target));
    };

    render = () => {
        const { isSaving, isSaveFailed } = this.props;

        return (
            <section className="new-post bordered">
                <form className="form" onSubmit={ this.handleSubmit }>
                    <h2>New blog post</h2>

                    {isSaveFailed && <h3>Failed</h3>}

                    <div className="row" style={{ opacity: isSaving ? 0.2 : 1 }}>
                        <div className="span-2-of-3 col-fl">
                            <input name="title" type="text" className="form-element" placeholder="My post title" required />
                            <textarea name="description" className="form-element" placeholder="Post description" required>
                            </textarea>
                            <input name="email" type="email" className="form-element" placeholder="E-Mail Address" required />
                            <input name="image" type="file" className="form-element" />
                        </div>
                        <div className="span-1-of-3 col-fl">
                            <input type="submit" value="Save post" />
                        </div>
                    </div>
                </form>
            </section>
        )
    }

}

NewPost.propTypes = {
    isSaving: PropTypes.bool.isSaving,
    isSaveFailed: PropTypes.bool.isSaveFailed,
    data: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    isSaving: state.newPost.isSaving,
    isSaveFailed: state.newPost.isSaveFailed,
    data: state.newPost.data
});

const mapDispatchToProps = {
    addPost: addPost
};

export default connect(mapStateToProps, mapDispatchToProps)(NewPost)
