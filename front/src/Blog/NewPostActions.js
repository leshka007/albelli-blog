import fetch from 'isomorphic-fetch'
import moment from 'moment'
import { fetchPosts } from './BlogActions'

export const BLOG_ADD_POST_REQUEST = 'BLOG_ADD_POST_REQUEST';
export const BLOG_ADD_POST_SUCCESS = 'BLOG_ADD_POST_SUCCESS';
export const BLOG_ADD_POST_FAILED = 'BLOG_ADD_POST_FAILED';

export const addPostRequest = () => ({
    type: BLOG_ADD_POST_REQUEST
});

export const addPostSuccess = () => ({
    type: BLOG_ADD_POST_SUCCESS
});

export const addPostFailed = () => ({
    type: BLOG_ADD_POST_FAILED
});

export const addPost = (data) => {
    console.log(data);
    return dispatch => {
        dispatch(addPostRequest());
        fetch('/api/posts/add', {
            method: 'POST',
            body: data,
        })
            .then((json) => {
                dispatch(addPostSuccess());
                dispatch(fetchPosts());
            })
            .catch((err) => {
                dispatch(addPostFailed());
            });
    }
};
