import { BLOG_ADD_POST_REQUEST, BLOG_ADD_POST_SUCCESS, BLOG_ADD_POST_FAILED } from './NewPostActions';

const newPostReducer = (
    state = {
        isSaving: false,
        isSaveFailed: false,
        data: [],
    },
    action
) => {
    switch (action.type) {
        case BLOG_ADD_POST_REQUEST:
            return Object.assign({}, state, {
                isSaving: true,
                isSaveFailed: false,
                data: []
            });
        case BLOG_ADD_POST_SUCCESS:
            return Object.assign({}, state, {
                isSaving: false,
                data: action.data,
            });
        case BLOG_ADD_POST_FAILED:
            return Object.assign({}, state, {
                isSaving: false,
                isSaveFailed: true
            });
        default:
            return state;
    }
};

export default newPostReducer
