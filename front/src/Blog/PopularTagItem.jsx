import React, { Component } from 'react'
import PropTypes from 'prop-types'

class PopularTagItem extends Component {

    render = () => (
        <li>
            {this.props.title}
        </li>
    )

}

PopularTagItem.propTypes = {
    title: PropTypes.string.isRequired
};

export default PopularTagItem
