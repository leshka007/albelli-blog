import React, { Component } from 'react';
import {connect} from "react-redux";
import PropTypes from 'prop-types'
import PopularTagItem from "./PopularTagItem";

class PopularTagList extends Component {

    render = () => {
        const { isFetchFailed, isFetching, tags } = this.props;

        return (
            <section className="popular-tags bordered clearfix">
                <span className="tags-title">Most used words here</span>
                {isFetchFailed && <h2>Failed</h2>}
                {isFetching && tags.length === 0 && <h2>Loading...</h2>}
                {!isFetchFailed && !isFetching && tags.length === 0 && <h2>Empty.</h2>}
                {tags.length > 0 &&
                <ul>
                    {tags.map((tagTitle, index) => <PopularTagItem key={index} title={tagTitle}/>)}
                </ul>}
            </section>
        )
    }

}

PopularTagList.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    isFetchFailed: PropTypes.bool.isRequired,
    tags: PropTypes.array.isRequired
}

const mapStateToProps = (state) => ({
    isFetching: state.blog.isFetching,
    isFetchFailed: state.blog.isFetchFailed,
    tags: state.blog.tags
});

export default connect(mapStateToProps)(PopularTagList)
