import React, { Component } from 'react'
import PropTypes from 'prop-types'

class PostItem extends Component {
    render = () => (
        <article className="post bordered">
            <div className="clearfix">
                <span className="post-date">{ this.props.data.createdAt.format('D/M/Y') }</span>
                <h2 className="post-title">{ this.props.data.title }</h2>
            </div>

            <div className="clearfix">
                { this.props.data.image && <img src={ this.props.data.image } className="blog-thumb" /> }
                <div className="descr">
                    {this.props.data.description}
                </div>
            </div>
        </article>
    )
}

PostItem.propTypes = {
    data: PropTypes.object.isRequired
}

export default PostItem
