import React, { Component } from 'react';
import PostItem from "./PostItem.jsx";
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { fetchPosts } from './BlogActions'

class PostList extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount = () => {
        this.props.fetch()
    };

    render = () => {
        const { isFetchFailed, isFetching, posts } = this.props;

        return (
            <section className="post-list">
                {isFetchFailed && <h2>Failed</h2>}
                {isFetching && posts.length === 0 && <h2>Loading...</h2>}
                {!isFetchFailed && !isFetching && posts.length === 0 && <h2>Empty.</h2>}
                {posts.length > 0 &&
                <div style={{ opacity: isFetching ? 0.2 : 1 }}>
                    { posts.map((data, index) => <PostItem key={index} data={data} />) }
                </div>}
            </section>
        )
    }
}

PostList.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    isFetchFailed: PropTypes.bool.isRequired,
    posts: PropTypes.array.isRequired
};

const mapStateToProps = (state) => ({
    isFetching: state.blog.isFetching,
    isFetchFailed: state.blog.isFetchFailed,
    posts: state.blog.posts
});

const mapDispatchToProps = {
    fetch: fetchPosts
};

export default connect(mapStateToProps, mapDispatchToProps)(PostList)
