import React from 'react'

const Header = () => (
    <header>
        <h1>My spectacular blog</h1>
        <i>A totally false statement</i>
    </header>
)

export default Header
