import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import blogReducer from './Blog/BlogReducer';
import newPostReducer from './Blog/NewPostReducer';

const loggerMiddleware = createLogger();

const store =  createStore(
    combineReducers({
        blog: blogReducer,
        newPost: newPostReducer
    }),
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    )
);

export default store
