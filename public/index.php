<?php

include_once __DIR__ . '/../vendor/autoload.php';

$routes = include_once __DIR__ . '/../config/routes.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new \Albelli\Blog\Application();

$request = Request::createFromGlobals();

$requestMethod = $request->getMethod();
$requestPath = $request->getPathInfo();

$response = null;
foreach ($routes as $route) {
    list($method, $path, $handler) = $route;
    if ($requestMethod == $method && $requestPath == $path) {
        $handler = $app->get($handler);
        $response = $handler($request);
    }
}
if (!$response) {
    $response = new Response('Not Found', 404);
}

$response->send();
