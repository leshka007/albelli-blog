<?php

declare(strict_types=1);

namespace Albelli\Blog;

class Application
{
    use ConfigureContainerTrait;

    /**
     * @var \DI\Container
     */
    private $container;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->container = $this->configureContainer();
    }

    /**
     * @param $name
     * @return mixed
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function get($name)
    {
        return $this->container->get($name);
    }

    public function call(callable $callable)
    {
        return $this->container->call($callable);
    }

}
