<?php

declare(strict_types=1);

namespace Albelli\Blog;

use DI\ContainerBuilder;

trait ConfigureContainerTrait
{

    protected function configureContainer()
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions(__DIR__ . '/../../config/config.php');
        $containerBuilder->addDefinitions(__DIR__ . '/../../config/services.php');
        $container = $containerBuilder->build();
        return $container;
    }

}
