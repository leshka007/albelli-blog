<?php

declare(strict_types=1);

namespace Albelli\Blog\Domain;

use InvalidArgumentException;

/**
 * Interface DomainInterface
 * @package Albelli\Blog\Domain
 */
interface DomainInterface
{

    /**
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function validate();

    public function asArray();

    public static function fromArray(array $array);

}
