<?php

declare(strict_types=1);

namespace Albelli\Blog\Domain;

use InvalidArgumentException;
use Nette\Utils\Arrays;
use Nette\Utils\Validators;
use Webmozart\Assert\Assert;

final class Post implements DomainInterface
{

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $image;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    public function __construct(string $title, string $description, string $email, string $image = '', \DateTimeImmutable $createdAt = null)
    {
        $this->title = $title;
        $this->description = $description;
        $this->email = $email;
        $this->image = $image;
        $this->createdAt = $createdAt ?? new \DateTimeImmutable();
        $this->validate();
    }

    /**
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function validate()
    {
        Assert::true(Validators::isEmail($this->email));
        Assert::stringNotEmpty($this->title);
        Assert::notEmpty( $this->description . $this->image ); // check for "Both the content and the image fields are empty"
        Assert::notEmpty($this->createdAt);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function asArray()
    {
        return [
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'email' => $this->getEmail(),
            'image' => $this->getImage() ? $this->getImage() : null,
            'createdAt' => $this->getCreatedAt()->getTimestamp()
        ];
    }

    public static function fromArray(array $array)
    {
        $title = Arrays::get($array, 'title');
        Assert::string($title);

        $description = Arrays::get($array, 'description');
        Assert::string($description);

        $email = Arrays::get($array, 'email');
        Assert::string($email);

        $image = Arrays::get($array, 'image');
        Assert::nullOrString($image);
        if (!$image) {
            $image = '';
        }

        $createdAt = Arrays::get($array, 'createdAt');
        Assert::integer($createdAt);

        $createdAt = (new \DateTimeImmutable())->setTimestamp(
            $createdAt
        );

        return new Post(
            $title,
            $description,
            $email,
            $image,
            $createdAt
        );
    }

}
