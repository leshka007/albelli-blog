<?php

declare(strict_types=1);

namespace Albelli\Blog\Domain;


use InvalidArgumentException;
use Nette\Utils\Arrays;
use Webmozart\Assert\Assert;

final class Tag implements DomainInterface
{

    /**
     * @var string
     */
    private $value;

    /**
     * @var int
     */
    private $weight;

    /**
     * Tag constructor.
     * @param string $value
     * @param int $weight
     */
    public function __construct(string $value, int $weight)
    {
        $this->value = $value;
        $this->weight = $weight;
        $this->validate();
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function validate()
    {
        Assert::notContains($this->value, ' ');
        Assert::greaterThan($this->weight, 0);
    }

    public function asArray()
    {
        return [
            'value' => $this->value,
            'weight' => $this->weight
        ];
    }

    public static function fromArray(array $array)
    {
        $value = Arrays::get($array, 'value');
        $weight = Arrays::get($array, 'weight');

        Assert::string($value);
        Assert::integer($weight);

        return new Tag($value, $weight);
    }

}
