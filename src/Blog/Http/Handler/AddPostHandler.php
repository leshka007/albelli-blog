<?php

declare(strict_types=1);

namespace Albelli\Blog\Http\Handler;

use Albelli\Blog\Domain\Post;
use Albelli\Blog\Image\Storage;
use Albelli\Blog\Repository\PostRepository;
use Albelli\Blog\Repository\TagRepository;
use Albelli\Blog\TagExtractor;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class AddPostHandler
{

    /**
     * @var PostRepository
     */
    private $postRepo;

    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * @var Storage
     */
    private $imageStorage;

    /**
     * @var TagExtractor
     */
    private $tagExtractor;

    /**
     * AddPostHandler constructor.
     * @param PostRepository $postRepo
     * @param TagRepository $tagRepository
     * @param Storage $imageStorage
     * @param TagExtractor $tagExtractor
     */
    public function __construct(
        PostRepository $postRepo,
        TagRepository $tagRepository,
        Storage $imageStorage,
        TagExtractor $tagExtractor
    )
    {
        $this->postRepo = $postRepo;
        $this->tagRepository = $tagRepository;
        $this->imageStorage = $imageStorage;
        $this->tagExtractor = $tagExtractor;
    }

    public function __invoke(Request $request)
    {
        $title = $request->request->get('title');
        $description = $request->request->get('description');
        $email = $request->request->get('email');
        $image = $request->files->get('image');

        try {
            if ($image) {
                $image = $this->imageStorage->upload($image);
            } else {
                $image = '';
            }
            $post = new Post($title, $description, $email, $image);

            $this->postRepo->add($post);

            $tags = $this->tagExtractor->extractFromPost($post);
            foreach ($tags as $tag)
            {
                $this->tagRepository->add($tag);
            }
            $this->tagRepository->flush();
        } catch (InvalidArgumentException $e) {
            return new JsonResponse(['error' => $e->getMessage()], 422);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
        return new JsonResponse([
            'title' => $title,
            'descr' => $description,
            'email' => $email,
            'image' => $image
        ]);
    }

}
