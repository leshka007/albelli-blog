<?php

declare(strict_types=1);

namespace Albelli\Blog\Http\Handler;

use Albelli\Blog\Domain\Post;
use Albelli\Blog\Domain\Tag;
use Albelli\Blog\Repository\PostRepository;
use Albelli\Blog\Repository\TagRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PostsHandler
{

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * PostsHandler constructor.
     */
    public function __construct(PostRepository $postRepository, TagRepository $tagRepository)
    {
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
    }

    public function __invoke(Request $request)
    {
        $numTags = $request->query->get('numTags', 5);

        $posts = $this->postRepository->fetchAll();
        $tags = $this->tagRepository->getTop($numTags);

        return new JsonResponse([
            'posts' => array_map(function(Post $post) {
                $post = $post->asArray();
                $post['image'] = $post['image'] ? '/images' . $post['image'] : null;
                return $post;
            }, $posts),
            'tags' => array_map(function(Tag $tag) {
                return $tag->getValue();
            }, $tags)
        ]);
    }

}
