<?php

declare(strict_types=1);

namespace Albelli\Blog\Image;

use InvalidArgumentException;
use Nette\Utils\Arrays;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;
use Nette\Utils\UnknownImageFileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Webmozart\Assert\Assert;

final class Storage
{

    /**
     * @var array
     */
    private $allowedMimeTypes = [ 'image/jpeg', 'image/png' ];

    /**
     * @var int
     */
    private $storeWidth;

    /**
     * @var int
     */
    private $storeHeight;

    /**
     * @var string
     */
    private $storePath;

    public function __construct(string $storePath, int $storeWidth = 300, int $storeHeight = 300)
    {
        $this->storeWidth = $storeWidth;
        $this->storeHeight = $storeHeight;
        $this->storePath = $storePath;
    }

    /**
     * @param UploadedFile $requestImage
     * @throws InvalidArgumentException
     */
    private function validateUploadedFile(UploadedFile $requestImage)
    {
        Assert::oneOf($requestImage->getMimeType(), $this->allowedMimeTypes);
    }

    private function generateName(UploadedFile $requestImage): string
    {
        $pathinfo = pathinfo($requestImage->getClientOriginalName());
        $filename = Arrays::get($pathinfo, 'filename');
        $ext = Arrays::get($pathinfo, 'extension');

        $id = 0;
        do {
            $savePath = $this->storePath . '/' . date('Ymd') . '/' . $filename . ($id++ > 0 ? '_' . $id : '') . '.' . $ext;
        } while (file_exists($savePath));
        return $savePath;
    }

    private function preparePath(string $savePath)
    {
        Filesystem::createDir(dirname($savePath));
    }

    public function upload(UploadedFile $requestImage): string
    {
        $this->validateUploadedFile($requestImage);
        try {
            $image = Image::fromFile($requestImage->getPathname());
        } catch (UnknownImageFileException $e) {
            throw new InvalidArgumentException('Unknown image file');
        }
        $image->resize($this->storeWidth, $this->storeHeight, Image::EXACT);
        $savePath = $this->generateName($requestImage);
        $this->preparePath($savePath);
        $image->save($savePath);
        return str_replace($this->storePath, '', $savePath);
    }

}
