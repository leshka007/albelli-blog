<?php

declare(strict_types=1);

namespace Albelli\Blog\Repository\Post;

use Albelli\Blog\Domain\Post;
use InvalidArgumentException;
use Webmozart\Assert\Assert;

final class GetPostsFromFileSystem
{

    /**
     * @var string
     */
    private $storePath;

    public function __construct(string $storePath)
    {
        $this->storePath = $storePath;
    }

    private function extractComparableValueFromPath(string $path): array
    {
        $path = explode('/', $path);
        $lastSlice = array_slice($path, -2);

        $datePart = intval($lastSlice[0]);
        $idPath = intval(substr($lastSlice[1], 0, strpos($lastSlice[1], '.')));

        return [$datePart, $idPath];
    }

    private function loadPosts(array $pathes): array
    {
        $posts = [];
        foreach ($pathes as $path) {
            try {
                $arr = json_decode(file_get_contents($path), true);
                if (is_array($arr)) {
                    $posts[] = Post::fromArray($arr);
                }
            } catch (\Exception $e) {
            }
        }
        return $posts;
    }

    public function __invoke(int $limit = null, int $offset = 0): array
    {
        $patches = glob($this->storePath . '/*/*.json');
        $patches = array_filter($patches, function($path) {
            try {
                Assert::regex($path, '/[\d]{8}\/[\d]+\.json$/');
            } catch (InvalidArgumentException $e) {
                return false;
            }
            return true;
        });

        usort($patches, function($a, $b) {
            $a = $this->extractComparableValueFromPath($a);
            $b = $this->extractComparableValueFromPath($b);
            for ($i=0; $i<count($a); $i++) {
                if ($a[$i] == $b[$i]) {
                    continue;
                }
                return ($a[$i] < $b[$i]) ? 1 : -1;
            }
            return 0;
        });

        if ($offset > 0) {
            $patches = array_slice($patches, $offset);
        }
        if (!is_null($limit)) {
            $patches = array_slice($patches, 0, $limit);
        }

        return $this->loadPosts($patches);
    }

}
