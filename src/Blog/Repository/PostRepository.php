<?php

declare(strict_types=1);

namespace Albelli\Blog\Repository;

use Albelli\Blog\Domain\Post;
use Albelli\Blog\Repository\Post\GetPostsFromFileSystem;
use Nette\Utils\FileSystem;

final class PostRepository
{

    /**
     * @var string
     */
    private $storePath;

    public function __construct(string $storePath)
    {
        $this->storePath = $storePath;
    }

    private function generatePath(Post $post)
    {
        $id = 0;
        do {
            $filePath = $this->storePath . '/' . $post->getCreatedAt()->format('Ymd') . '/' . ++$id . '.json';
        } while (file_exists($filePath));
        return $filePath;
    }

    private function preparePath(string $savePath)
    {
        Filesystem::createDir(dirname($savePath));
    }

    public function add(Post $post): bool
    {
        $savePath = $this->generatePath($post);
        $this->preparePath($savePath);
        return (bool)file_put_contents(
            $savePath,
            json_encode($post->asArray(), JSON_PRETTY_PRINT)
        );
    }

    public function fetchAll(int $limit = null, int $offset = 0)
    {
        $command = new GetPostsFromFileSystem($this->storePath);
        return $command($limit, $offset);
    }

}
