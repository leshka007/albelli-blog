<?php

declare(strict_types=1);

namespace Albelli\Blog\Repository\Tag;

use Albelli\Blog\Domain\Tag;

final class GetSavedTagsFromFileSystem
{
    /**
     * @var string
     */
    private $storePath;

    /**
     * GetSavedTags constructor.
     */
    public function __construct(string $storePath)
    {
        $this->storePath = $storePath;
    }

    /**
     * @return Tag[]
     */
    public function __invoke(): array
    {
        if (!file_exists($this->storePath)) {
            return [];
        }
        $data = file_get_contents($this->storePath);
        $tags = json_decode($data, true);
        if (!is_array($tags)) {
            return [];
        }
        $result = [];
        foreach ($tags as $tagData) {
            $result[] = Tag::fromArray($tagData);
        }
        return $result;
    }
}
