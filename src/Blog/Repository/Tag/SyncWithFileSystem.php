<?php

declare(strict_types=1);

namespace Albelli\Blog\Repository\Tag;

use Albelli\Blog\Domain\Tag;
use Webmozart\Assert\Assert;

final class SyncWithFileSystem
{

    /**
     * @var string
     */
    private $storePath;

    /**
     * SyncWithFileSystem constructor.
     */
    public function __construct(string $storePath)
    {
        $this->storePath = $storePath;
    }

    /**
     * @param Tag[] $tags
     */
    public function __invoke(array $tags)
    {
        $data = [];
        foreach ($tags as $tag) {
            Assert::isInstanceOf($tag, Tag::class);
            $data[] = $tag->asArray();
        }
        file_put_contents($this->storePath, json_encode($data, JSON_PRETTY_PRINT));
    }

}
