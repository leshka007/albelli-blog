<?php

declare(strict_types=1);

namespace Albelli\Blog\Repository;

use Albelli\Blog\Domain\Tag;
use Albelli\Blog\Repository\Tag\GetSavedTagsFromFileSystem;
use Albelli\Blog\Repository\Tag\SyncWithFileSystem;

final class TagRepository
{
    /**
     * @var string
     */
    private $storePath;

    /**
     * @var Tag[]
     */
    private $storage = [];

    /**
     * TagRepository constructor.
     */
    public function __construct(string $storePath)
    {
        $this->storePath = $storePath;
        $this->initWithSavedData();
    }

    private function initWithSavedData()
    {
        $command = new GetSavedTagsFromFileSystem($this->storePath);
        $tags = $command();
        foreach ($tags as $tag) {
            $this->storage[$tag->getValue()] = $tag;
        }
    }

    public function add(Tag $tag)
    {
        $weight = $tag->getWeight();
        $value = $tag->getValue();
        if (isset($this->storage[$value])) {
            $weight = $this->storage[$value]->getWeight() + 1;
        }
        $this->storage[$value] = new Tag($value, $weight);
    }

    public function flush()
    {
        $command = new SyncWithFileSystem($this->storePath);
        return $command($this->storage);
    }

    public function getTop(int $total): array
    {
        uasort($this->storage, function($a, $b) {
            if ($a->getWeight() == $b->getWeight()) {
                return 0;
            }
            return ($a->getWeight() < $b->getWeight()) ? 1 : -1;
        });

        return array_slice(array_values($this->storage), 0, $total);
    }

    public function clear()
    {
        $this->storage = [];
    }

}
