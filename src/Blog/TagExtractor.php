<?php

declare(strict_types=1);

namespace Albelli\Blog;

use Albelli\Blog\Domain\Post;
use Albelli\Blog\Domain\Tag;

final class TagExtractor
{

    /**
     * @var int
     */
    private $minLength;

    public function __construct(int $minLength = 4)
    {
        $this->minLength = $minLength;
    }

    public function extractFromString(string $text): array
    {
        $words = explode(' ', $text);
        $words = array_map(function($word) {
            return strtolower($word);
        }, $words);
        $words = array_filter($words, function($word) {
            return strlen($word) > $this->minLength;
        });
        return array_map(
            function($word) {
                return new Tag($word, 1);
            },
            $words
        );
    }

    public function extractFromPost(Post $post): array
    {
        return $this->extractFromString(
            $post->getTitle() . ' ' . $post->getDescription()
        );
    }

}
